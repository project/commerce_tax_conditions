<?php

/**
 * @file
 * Module hooks.
 */

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function commerce_tax_conditions_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the commerce_tax_conditions module.
    case 'help.page.commerce_tax_conditions':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides conditions for the product tax.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_form_alter().
 */
function commerce_tax_conditions_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $tax_type_form_ids = ['commerce_tax_type_edit_form', 'commerce_tax_type_add_form'];
  if (in_array($form_id, $tax_type_form_ids)) {
    /** @var \Drupal\commerce_tax\Entity\TaxTypeInterface $tax_type */
    $tax_type = $form_state->getFormObject()->getEntity();
    $product_tax_manager = \Drupal::service('commerce_tax_conditions.manager');
    $conditions = $product_tax_manager->getTaxTypeConditions($tax_type);
    $form['commerce_conditions'] = [
      '#type' => 'commerce_conditions',
      '#title' => t('Conditions'),
      '#default_value' => $conditions,
      '#parent_entity_type' => 'commerce_tax_type',
      '#entity_types' => ['commerce_order_item'],
    ];
    $form['actions']['submit']['#submit'][] = '_commerce_tax_conditions_save_conditions';
  }
}

/**
 * Save tax type conditions.
 */
function _commerce_tax_conditions_save_conditions(array $form, FormStateInterface $form_state) {
  $conditions = $form_state->getValue('commerce_conditions');
  $conditions_to_save = [];
  foreach ($conditions as $condition) {
    $conditions_to_save[] = [
      'plugin' => $condition['plugin'],
      'configuration' => Json::encode($condition['configuration']),
    ];
  }
  if ($conditions_to_save) {
    /** @var \Drupal\commerce_tax\Form\TaxTypeForm $form_entity */
    $form_entity = $form_state->getFormObject();
    /** @var \Drupal\commerce_tax\Entity\TaxTypeInterface $tax_type */
    $tax_type = $form_entity->getEntity();
    $tax_type->setThirdPartySetting('commerce_tax_conditions', 'conditions', $conditions_to_save);
    $tax_type->save();
  }
}

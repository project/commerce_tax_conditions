<?php

namespace Drupal\commerce_tax_conditions\Plugin\Commerce\Condition;

use Drupal\commerce_order\Plugin\Commerce\Condition\OrderCustomerRole;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ExemptCondition.
 *
 * @CommerceCondition(
 *   id = "customer_role",
 *   label = @Translation("Customer role"),
 *   category = @Translation("Customer"),
 *   parent_entity_type = "commerce_tax_type",
 *   entity_type = "commerce_order_item",
 * )
 */
class CustomerRoleCondition extends OrderCustomerRole {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'negate' => NULL,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['negate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Negate the roles condition'),
      '#description' => $this->t('If checked, the value(s) selected should not match.'),
      '#default_value' => $this->configuration['negate'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['negate'] = $values['negate'];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = $entity;
    $customer = $order_item->getOrder()->getCustomer();

    $result = (bool) array_intersect($this->configuration['roles'], $customer->getRoles());

    if ($this->configuration['negate']) {
      return !$result;
    }

    return $result;
  }

}

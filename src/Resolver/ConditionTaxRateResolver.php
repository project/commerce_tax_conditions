<?php

namespace Drupal\commerce_tax_conditions\Resolver;

use Drupal\commerce\ConditionManagerInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_tax_conditions\Service\CommerceTaxConditionsManager;
use Drupal\commerce_tax\Resolver\TaxRateResolverInterface;
use Drupal\commerce_tax\Resolver\TaxTypeAwareInterface;
use Drupal\commerce_tax\Resolver\TaxTypeAwareTrait;
use Drupal\commerce_tax\TaxRate;
use Drupal\commerce_tax\TaxZone;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Class ConditionTaxRate.
 */
class ConditionTaxRateResolver implements TaxRateResolverInterface, TaxTypeAwareInterface {

  use TaxTypeAwareTrait;

  /**
   * ConditionTaxRate constructor.
   */
  public function __construct(private ConditionManagerInterface $conditionManager, private CommerceTaxConditionsManager $conditionsTaxManager) {}

  /**
   * Check if our tax types have saved conditions.
   */
  public function apply(TaxZone $zone, OrderItemInterface $order_item, ProfileInterface $customer_profile): bool {
    $tax_conditions = $this->conditionsTaxManager->getTaxTypeConditions($this->taxType);
    if ($tax_conditions) {
      foreach ($tax_conditions as $tax_condition) {
        /** @var \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface $condition_plugin */
        $condition_plugin = $this->conditionManager->createInstance($tax_condition['plugin'], $tax_condition['configuration']);
        if (!$condition_plugin->evaluate($order_item)) {
          return FALSE;
        }
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(TaxZone $zone, OrderItemInterface $order_item, ProfileInterface $customer_profile) {
    $rate = NULL;
    $tax_type_configs = $this->taxType->getPluginConfiguration();
    if (!empty($tax_type_configs['rates'][0]['id'])) {
      $type_rate_id = $tax_type_configs['rates'][0]['id'];
      $rate = $zone->getRate($type_rate_id);
    }

    if ($rate instanceof TaxRate) {
      if (!$this->apply($zone, $order_item, $customer_profile)) {
        return static::NO_APPLICABLE_TAX_RATE;
      }
    }

    return $rate;
  }

}

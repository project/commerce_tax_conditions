<?php

namespace Drupal\commerce_tax_conditions\Service;

use Drupal\commerce_tax\Entity\TaxTypeInterface;
use Drupal\Component\Serialization\Json;

/**
 * Commerce tax conditions manager.
 */
class CommerceTaxConditionsManager {

  /**
   * Get conditions from tax type.
   */
  public function getTaxTypeConditions(TaxTypeInterface $tax_type): array {
    $conditions = $tax_type->getThirdPartySetting('commerce_tax_conditions', 'conditions') ?: [];
    foreach ($conditions as &$condition) {
      $condition['configuration'] = Json::decode($condition['configuration']);
    }

    return $conditions;
  }

}

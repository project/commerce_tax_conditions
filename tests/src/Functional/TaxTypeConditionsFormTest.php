<?php

namespace Drupal\Tests\commerce_tax_conditions\Functional;

use Drupal\Tests\commerce\Functional\CommerceBrowserTestBase;

/**
 * Test commerce tax form with conditions.
 *
 * @package Drupal\Tests\commerce_tax_conditions\Functional
 *
 * @group commerce_tax_conditions
 */
class TaxTypeConditionsFormTest extends CommerceBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce',
    'commerce_product',
    'commerce_tax',
    'commerce_tax_conditions',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce_tax_type',
    ], parent::getAdministratorPermissions());
  }

  /**
   * Test condition element on the form.
   */
  public function testFormElement() {
    $this->drupalGet('admin/commerce/config/tax-types/add');
    $edit = [
      'label' => 'Tax type',
      'id' => 'tax_type',
      'plugin' => 'custom',
      'configuration[custom][rates][0][rate][label]' => 'custom rate',
      'status' => 1,
    ];
    $this->drupalGet('admin/commerce/config/tax-types/add');
    $this->assertSession()->responseContains('Conditions');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Saved the Tax type tax type');
  }

}

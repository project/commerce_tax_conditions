<?php

namespace Drupal\Tests\commerce_tax_conditions\Unit;

use Drupal\commerce_tax\Entity\TaxType;
use Drupal\commerce_tax_conditions\Service\CommerceTaxConditionsManager;
use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;

/**
 * Unit test for tax conditions manager.
 *
 * @package Drupal\Tests\commerce_tax_conditions\Unit
 *
 * @group commerce_tax_conditions
 */
class TaxExemptManagerTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $container = new ContainerBuilder();
    $taxExemptManager = new CommerceTaxConditionsManager();
    $container->set('commerce_tax_conditions.manager', $taxExemptManager);
    \Drupal::setContainer($container);
  }

  /**
   * Checks if the service is created in the Drupal context.
   */
  public function testTaxExemptManager() {
    $this->assertNotNull(\Drupal::service('commerce_tax_conditions.manager'));
  }

  /**
   * Test fetching conditions from the tax type.
   */
  public function testFetchConditions() {
    $tax_type = new TaxType(['plugin' => 'default'], 'default');
    $conditions_to_save = [
      [
        'plugin' => 'test_plugin',
        'configuration' => Json::encode(['user_role' => 'admin', 'custom_config' => 'test']),
      ],
    ];
    $expected_conditions = [
      [
        'plugin' => 'test_plugin',
        'configuration' => ['user_role' => 'admin', 'custom_config' => 'test'],
      ],
    ];

    $tax_type->setThirdPartySetting('commerce_tax_conditions', 'conditions', $conditions_to_save);
    $saved_conditions = \Drupal::service('commerce_tax_conditions.manager')->getTaxTypeConditions($tax_type);
    $this->assertEquals($expected_conditions, $saved_conditions);
  }

}


## Contents Of This File

-  Introduction
-  Requirements
-  Installation
-  Configuration
-  Maintainers


## Introduction

This module provides conditions for the product tax exempt.
Adds UI for selecting conditions for Product Tax.

-  For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_tax_conditions

-  To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/commerce_tax_conditions


## Requirements

-  Commerce Tax


## Installation

-  Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


## Configuration

- No configuration is needed.


## Maintainers

Current maintainers:

-  Mykhailo Gurei (ozin) - https://www.drupal.org/u/ozin
